package client.widgets.expandablerecyclerview;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import client.widgets.expandablerecyclerview.base.ChildViewHolder;

public class ChildrenViewHolder extends ChildViewHolder<Child> {

    public final ViewDataBinding binding;

    public ChildrenViewHolder(@NonNull ViewDataBinding dataBinding) {
        super(dataBinding.getRoot());
        binding = dataBinding;
    }
}
