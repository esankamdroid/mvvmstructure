package client.widgets.expandablerecyclerview;

import client.injection.components.IViewModelComponent;
import client.widgets.expandablerecyclerview.base.Parent;

public abstract class Header implements Parent<Child> {

    public abstract void injectMembers(IViewModelComponent activityComponent);

    public abstract int getBindingVariable();

    public boolean isParent() {
        return true;
    }

    public abstract void setViewHolder(HeaderViewHolder headerViewHolder);

    public Header(IViewModelComponent activityComponent) {
        injectMembers(activityComponent);
    }
}
