package client.widgets.expandablerecyclerview;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import client.widgets.expandablerecyclerview.base.ParentViewHolder;

public class HeaderViewHolder extends ParentViewHolder {

    public final ViewDataBinding binding;

    public HeaderViewHolder(@NonNull ViewDataBinding dataBinding) {
        super(dataBinding.getRoot());
        binding = dataBinding;
    }
}
