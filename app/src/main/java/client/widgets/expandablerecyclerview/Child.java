package client.widgets.expandablerecyclerview;

import client.injection.components.IViewModelComponent;

public abstract class Child {

    public abstract void injectMembers(IViewModelComponent activityComponent);

    public abstract int getBindingVariable();

    public boolean isChild() {
        return true;
    }

    public Child(IViewModelComponent activityComponent) {
        injectMembers(activityComponent);
    }
}
