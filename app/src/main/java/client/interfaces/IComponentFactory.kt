package client.interfaces

import client.injection.components.IViewModelComponent
import client.injection.modules.ActivityViewModelModule

interface IComponentFactory {

    fun createActivityViewModelModule(module: ActivityViewModelModule): IViewModelComponent
}
