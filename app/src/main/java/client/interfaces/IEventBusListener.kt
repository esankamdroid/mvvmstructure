package client.interfaces

/**
 * Abstract interface that defines any object that would like to listen to events fired by
 * IEventBus
 */
interface IEventBusListener {
    /**
     * Called by the eventBus when there is a new event
     * @param baseEvent
     */
    fun onEvent(baseEvent: IBaseEvent)
}
