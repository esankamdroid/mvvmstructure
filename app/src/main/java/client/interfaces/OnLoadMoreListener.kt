package client.interfaces

interface OnLoadMoreListener {
    fun onLoadMore(current_page: Int)
}