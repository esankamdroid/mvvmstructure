package client.interfaces

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager

interface IViewModelToActivity {

    val activity: AppCompatActivity

    val fragmentManager: FragmentManager?

    fun startActivity(activityClass: Class<out Activity>)

    fun startActivityWithBundle(activityClass: Class<out Activity>, bundle: Bundle)

    fun startActivityAndFinishThis(activityClass: Class<out Activity>)

    fun startActivityAndFinishThis(intentToStart: Intent)

    fun showAppDetailSetting()

    fun finish()

    fun showToast(message: String, duration: Int)

    fun setSupportActionToolbar(toolbar: Toolbar)

    fun getString(stringIdFromResources: Int): String?

    fun getString(stringIdFromResources: Int, vararg formatArgs: Any): String?

    fun getColor(colorIdFromResources: Int): Int

    fun getDrawable(@DrawableRes id: Int): Drawable
}
