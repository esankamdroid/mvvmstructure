package client.interfaces


import io.reactivex.disposables.Disposable

/**
 * Abstract interface that defines the event bus that can be used by a component to get
 * or send events to the various components in the application.
 */
interface IEventBus : IBaseModule {

    /**
     * Call this method to fire an event asynchronously. The event will be delivered
     * to all the listeners on the main thread
     * @param event event to fire
     */
    fun send(event: IBaseEvent)

    /**
     * Call this method to register an event listener. All the events will be delivered to the
     * event listener on the main thread.
     * @param eventBusListener event listener
     * @return Subscription that marks the registration.
     * To de-register an event listener, un-subscribe on the subscription returned.
     */
    fun subscribe(eventBusListener: IEventBusListener): Disposable

}
