package client.interfaces

import client.injection.components.IViewModelComponent
import client.viewmodel.SplashViewModel

interface IViewModelFactory {

    fun createSplashViewModel(component: IViewModelComponent): SplashViewModel

}
