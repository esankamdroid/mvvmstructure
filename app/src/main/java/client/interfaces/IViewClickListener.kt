package client.interfaces

interface IViewClickListener<T> {
    fun onClick(viewModel: T)
}