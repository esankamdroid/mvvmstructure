package client.interfaces

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import client.injection.modules.ApplicationModule

interface IApp {

    val applicationContext: android.content.ContextWrapper

    val activity: AppCompatActivity

    val applicationModule: ApplicationModule

    val platformLayer: IPlatformLayer

    fun initializeApplicationEssentials(c: Context)
}
