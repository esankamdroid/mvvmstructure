package client.interfaces

import client.injection.components.IPlatformComponent
import client.injection.components.ITaskComponent

interface IPlatformLayer {

    val applicationComponent: IPlatformComponent

    val taskComponent: ITaskComponent

    fun cleanup()
}
