package client.interfaces

interface IWebClient : IBaseModule {

    fun <T> createWebService(service: Class<T>): T
}
