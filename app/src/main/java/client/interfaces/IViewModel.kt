package client.interfaces

import android.os.Bundle

interface IViewModel {

    fun getDataFromBundle(bundle: Bundle)

    fun onStart()

    fun onResume()

    fun onPause()

    fun onStop()

    fun onDestroy()
}
