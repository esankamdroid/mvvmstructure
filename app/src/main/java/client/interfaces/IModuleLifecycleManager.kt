package client.interfaces

interface IModuleLifecycleManager {
    fun registerModule(module: IBaseModule)

    fun resetAndRemoveModules()
}