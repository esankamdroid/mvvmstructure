package client.interfaces

interface IBaseEvent {
    fun isEventOfPassedType(eventType: Class<*>): Boolean

}
