package client.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableField
import client.base.BaseViewModel
import client.injection.components.IViewModelComponent

class SplashViewModel(iViewModelComponent: IViewModelComponent) : BaseViewModel(iViewModelComponent) {

    val name = ObservableField<String>()

    override fun getDataFromBundle(bundle: Bundle) {

    }

    override fun injectMember(activityComponent: IViewModelComponent) {
        activityComponent.inject(this)
    }

    init {
        name.set("This is Splash")
    }
}