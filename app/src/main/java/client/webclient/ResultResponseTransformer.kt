package client.webclient

import client.utils.NetworkUtils
import client.utils.Trace
import client.webclient.exception.*
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result
import java.io.IOException
import java.net.SocketTimeoutException

class ResultResponseTransformer<T> : ObservableTransformer<Result<T>, Response<T>> {

    override fun apply(upstream: Observable<Result<T>>): ObservableSource<Response<T>> {
        return upstream.flatMap { tResult -> processNetworkResponse(tResult) }
    }

    private fun processNetworkResponse(r: Result<T>): Observable<Response<T>> {
        var returnObservable: Observable<Response<T>>
        if (r.response() != null) {
            returnObservable = Observable.just(r.response()!!)
            if (r.isError) {
                val throwable = r.error()
                if (throwable is IOException) {
                    Trace.throwable(throwable, "Retrofit connection error.")
                    if (throwable is java.net.ConnectException) {
                        returnObservable = Observable.error(HttpNoInternetConnectionException(throwable))
                    } else if (throwable is SocketTimeoutException) {
                        returnObservable = Observable.error(HttpServerDownException(throwable))
                    } else {
                        returnObservable = Observable.error(HttpNoInternetConnectionException(throwable))
                    }
                } else {
                    Trace.error("Retrofit general error - fatal => %s", throwable!!.message!!)
                    Trace.throwable(throwable)
                    returnObservable = Observable.error(HttpGeneralErrorException(r.error()!!))
                }
            } else {
                val retrofitResponse = r.response()
                if (!retrofitResponse!!.isSuccessful) {
                    val code = retrofitResponse.code()
                    var message = ""
                    if (retrofitResponse.errorBody() != null) {
                        try {
                            message = retrofitResponse.errorBody()!!.string()
                        } catch (e: IOException) {
                            message = "Error reading errorBody from response"
                        }

                    }
                    Trace.info("Server responded with error. Code: %d, message = %s", code, message)
                    var t: Throwable? = null
                    if (NetworkUtils.isClientAuthenticationError(code)) {
                        t = HttpClientAuthenticationError(retrofitResponse.code(), message)
                    } else if (NetworkUtils.isClientError(code)) {
                        t = HttpClientErrorException(retrofitResponse.code(), message)
                    } else if (NetworkUtils.isServiceError(code)) {
                        t = HttpServerErrorException(retrofitResponse.code(), message)
                    } else
                        t = HttpGeneralErrorException(NullPointerException("throwable not found"))

                    returnObservable = Observable.error(t)
                    Trace.throwable(t)
                }
            }
        } else {
            returnObservable = Observable.error(NullPointerException("Response is null"))
        }
        return returnObservable
    }
}
