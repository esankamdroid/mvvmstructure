package client.webclient

import client.interfaces.IWebClient
import client.utils.CommonPlatformConstants
import client.utils.SystemUtils
import client.utils.Trace
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import javax.inject.Inject
import java.util.concurrent.TimeUnit

class WebClient @Inject constructor() : IWebClient {

    private var retrofit: Retrofit? = null

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(CommonPlatformConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(buildGson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(buildHttpClient())
            .build()
    }

    override fun <T> createWebService(service: Class<T>): T {
        return retrofit!!.create(service)
    }

    private fun buildGson(): Gson {
        return GsonBuilder().serializeNulls().registerTypeAdapterFactory(ItemTypeAdapterFactory())
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()
    }

    private fun buildHttpClient(): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.connectTimeout(1, TimeUnit.MINUTES)
        okHttpClientBuilder.writeTimeout(1, TimeUnit.MINUTES)
        okHttpClientBuilder.readTimeout(1, TimeUnit.MINUTES)

        if (!SystemUtils.isInstalledFromMarketPlace) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpClientBuilder.addInterceptor(interceptor)
        }
        return okHttpClientBuilder.build()
    }

    override fun cleanupModule() {
        Trace.logFunc()
        retrofit = null
    }
}
