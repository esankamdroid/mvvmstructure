package client.webclient.exception

import java.util.Locale

class HttpServerErrorException(internal val mHttpResultCode: Int, internal val mMessage: String) : BaseException(null) {

    override fun toString(): String {
        return String.format(
            Locale.getDefault(),
            "Http Server Error Exception. Http Error = %d. Message = %s.",
            mHttpResultCode,
            mMessage
        )
    }
}
