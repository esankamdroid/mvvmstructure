package client.webclient.exception

import java.util.Locale

class HttpClientAuthenticationError(val mHttpResultCode: Int, val mMessage: String) :
    BaseException(null) {

    override fun toString(): String {
        return String.format(
            Locale.getDefault(),
            "Http Client Auth Exception. Http Error = %d. Message = %s.",
            mHttpResultCode,
            mMessage
        )
    }

}