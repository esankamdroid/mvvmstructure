package client.webclient.exception

class HttpNoInternetConnectionException(e: Throwable) : BaseException(e)
