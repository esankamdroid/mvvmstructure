package client.webclient.exception

class HttpServerDownException(e: Throwable) : BaseException(e)
