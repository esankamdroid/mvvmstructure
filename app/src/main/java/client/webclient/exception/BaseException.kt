package client.webclient.exception

/**
 * Defines all the http exceptions that the failure web requests are converted into
 */
open class BaseException(val mError: Throwable?) : Exception() {

    override val message: String?
        get() =
            if (this.mError != null) {
                mError.message!!
            } else {
                toString()
            }

}

