package client.webclient.exception

import java.util.*

class HttpClientErrorException(val httpErrorCode: Int, internal val mMessage: String) : BaseException(null) {

    override fun toString(): String {
        return String.format(
            Locale.US, "Http Client Error Exception. Http Error = %d. Message = %s",
            httpErrorCode,
            mMessage
        )
    }

    override val message: String?
        get() = toString()
}