package client.binding


import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import client.base.BaseRecyclerViewItemViewModel
import client.base.BindingRecyclerViewAdapter
import client.base.ExpandableAdapter
import client.interfaces.IViewClickListener
import client.interfaces.OnLoadMoreListener
import client.utils.EndlessRecyclerOnScrollListener
import client.utils.ItemOffsetDecoration
import client.widgets.expandablerecyclerview.Header
import com.client.android.R


object RecyclerViewBindings {

    private val KEY_CLICK_HANDLER = -124
    private var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener? = null

    @JvmStatic
    @BindingAdapter("initAdapterAndSetItems")
    fun <T : BaseRecyclerViewItemViewModel> initAdapterAndSetItems(recyclerView: RecyclerView, items: Collection<T>) {
        recyclerView.isNestedScrollingEnabled = true
        if (recyclerView.layoutManager == null)
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        var adapter = recyclerView.adapter as BindingRecyclerViewAdapter<T>?
        if (adapter == null) {
            adapter = BindingRecyclerViewAdapter(items)
            setClickHandlersOnAdapter(adapter, recyclerView)
            recyclerView.adapter = adapter
        } else {
            adapter.setItems(items)
        }
    }

    @JvmStatic
    @BindingAdapter("initHorizontalAdapterAndSetItems")
    fun <T : BaseRecyclerViewItemViewModel> initHorizontalAdapterAndSetItems(
        recyclerView: RecyclerView,
        items: Collection<T>
    ) {
        recyclerView.isNestedScrollingEnabled = true
        if (recyclerView.layoutManager == null)
            recyclerView.layoutManager =
                LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
        var adapter = recyclerView.adapter as BindingRecyclerViewAdapter<T>?
        if (adapter == null) {
            adapter = BindingRecyclerViewAdapter(items)
            setClickHandlersOnAdapter(adapter, recyclerView)
            recyclerView.adapter = adapter
        } else {
            adapter.setItems(items)
        }
    }

    @JvmStatic
    @BindingAdapter("initGridAdapterAndSetItems", "columns")
    fun <T : BaseRecyclerViewItemViewModel> initGridAdapterAndSetItems(
        recyclerView: RecyclerView,
        items: Collection<T>, columns: Int
    ) {
        recyclerView.isNestedScrollingEnabled = true
        if (recyclerView.layoutManager == null) {
            recyclerView.layoutManager =
                GridLayoutManager(recyclerView.context, columns)
            val itemDecoration = ItemOffsetDecoration(recyclerView.context, R.dimen.offset)
            recyclerView.addItemDecoration(itemDecoration)
        }
        var adapter = recyclerView.adapter as BindingRecyclerViewAdapter<T>?
        if (adapter == null) {
            adapter = BindingRecyclerViewAdapter(items)
            setClickHandlersOnAdapter(adapter, recyclerView)
            recyclerView.adapter = adapter
        } else {
            adapter.setItems(items)
        }
    }

    @JvmStatic
    @BindingAdapter("initBothAdapterAndSetItems", "columns", "isGrid", "loadMore")
    fun <T : BaseRecyclerViewItemViewModel> initBothAdapterAndSetItems(
        recyclerView: RecyclerView,
        items: Collection<T>, columns: Int, isGrid: Boolean, onLoadMoreListener: OnLoadMoreListener
    ) {
        if (isGrid) {
            initGridAdapterAndSetItems(recyclerView, items, columns)
            recyclerView.layoutManager = GridLayoutManager(recyclerView.context, columns)
        } else {
            initAdapterAndSetItems(recyclerView, items)
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        }
        loadMoreListener(recyclerView, onLoadMoreListener)
    }

    @JvmStatic
    @BindingAdapter("initExpandableAdapterAndSetItems", "parentBinding", "childBinding")
    fun initExpandableAdapterAndSetItems(
        recyclerView: RecyclerView,
        items: Collection<Header>,
        parent: Int, child: Int
    ) {
        recyclerView.isNestedScrollingEnabled = true
        var adapter = recyclerView.adapter as ExpandableAdapter?
        if (adapter == null) {
            adapter = ExpandableAdapter(recyclerView.context, items, parent, child)
            recyclerView.adapter = adapter
        } else {
            adapter.setItems(items)
        }
        if (recyclerView.layoutManager == null)
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    }

    @JvmStatic
    @BindingAdapter("initAdapterAndSetItemsWithoutClickEvent")
    fun <T : BaseRecyclerViewItemViewModel> setItemsWithoutClickEvent(
        recyclerView: RecyclerView,
        items: Collection<T>
    ) {
        recyclerView.isNestedScrollingEnabled = true
        var adapter = recyclerView.adapter as BindingRecyclerViewAdapter<T>?
        if (adapter == null) {
            adapter = BindingRecyclerViewAdapter(items)
            recyclerView.adapter = adapter
        } else {
            adapter.setItems(items)
        }
    }

    @JvmStatic
    @BindingAdapter("initAdapterAndSetItemsWithNestedScroll")
    fun <T : BaseRecyclerViewItemViewModel> initAdapterAndSetItemsWithNestedScroll(
        recyclerView: RecyclerView,
        items: Collection<T>
    ) {
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.setHasFixedSize(false)
        var adapter = recyclerView.adapter as BindingRecyclerViewAdapter<T>?
        if (adapter == null) {
            adapter = BindingRecyclerViewAdapter(items)
            setClickHandlersOnAdapter(adapter, recyclerView)
            recyclerView.adapter = adapter
        } else {
            adapter.setItems(items)
        }
    }

    @JvmStatic
    @BindingAdapter("clickHandler")
    fun <T : BaseRecyclerViewItemViewModel> setHandler(recyclerView: RecyclerView, handler: IViewClickListener<T>) {
        val adapter = recyclerView.adapter as BindingRecyclerViewAdapter<T>?
        if (adapter != null) {
            adapter.setClickHandler(handler)
        } else {
            recyclerView.setTag(KEY_CLICK_HANDLER, handler)
        }
    }

    @JvmStatic
    @BindingAdapter("loadMoreListener")
    fun loadMoreListener(
        recyclerView: RecyclerView, onLoadMoreListener: OnLoadMoreListener
    ) {
        if (recyclerView.layoutManager != null) {
            if (endlessRecyclerOnScrollListener == null) {
                endlessRecyclerOnScrollListener = EndlessRecyclerOnScrollListener(
                    recyclerView.layoutManager!!,
                    onLoadMoreListener
                )
            }
            recyclerView.addOnScrollListener(endlessRecyclerOnScrollListener!!)
        }
    }

    fun <T : BaseRecyclerViewItemViewModel> setClickHandlersOnAdapter(
        adapter: BindingRecyclerViewAdapter<T>,
        recyclerView: RecyclerView
    ) {
        val clickHandler = recyclerView.getTag(KEY_CLICK_HANDLER)
        if (clickHandler != null && clickHandler is IViewClickListener<*>) {
            adapter.setClickHandler(clickHandler as IViewClickListener<T>)
        }

    }
}