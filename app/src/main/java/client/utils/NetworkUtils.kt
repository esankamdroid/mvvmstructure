package client.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities


object NetworkUtils {

    fun isClientError(httpErrorCode: Int): Boolean {
        return httpErrorCode >= 400 && httpErrorCode <= 499
    }

    fun isClientAuthenticationError(httpErrorCode: Int): Boolean {
        return httpErrorCode == 401
    }

    fun isServiceError(httpErrorCode: Int): Boolean {
        return httpErrorCode >= 500 && httpErrorCode <= 599
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork
        val capabilities = connectivityManager.getNetworkCapabilities(network)
        return capabilities != null && (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || capabilities.hasTransport(
            NetworkCapabilities.TRANSPORT_CELLULAR))
    }
}
