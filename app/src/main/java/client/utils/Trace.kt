package client.utils

import android.widget.Toast
import client.App
import timber.log.Timber

object Trace {

    @JvmStatic
    fun verbose(message: String, vararg args: Any) {
        Timber.tag(CommonPlatformConstants.TAG).v(message, *args)
    }

    @JvmStatic
    fun info(message: String, vararg args: Any) {
        Timber.tag(CommonPlatformConstants.TAG).i(message, *args)
    }

    @JvmStatic
    fun infoToast(message: String, vararg args: Any) {
        info(message, *args)
    }

    @JvmStatic
    fun toast(message: String) {
        Toast.makeText(App.instance.applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    @JvmStatic
    fun warn(message: String, vararg args: Any) {
        Timber.tag(CommonPlatformConstants.TAG).w(message, *args)
    }

    @JvmStatic
    fun error(message: String, vararg args: Any) {
        // On Android there is an extra stack frame that gets the stack trace.
        val stackFrameToPrint = if (SystemUtils.isRunningOnAndroid) 3 else 2

        val stack = Thread.currentThread().stackTrace
        var errorLocationStr = ""
        if (stack != null && stack.size > stackFrameToPrint + 1) {
            // stack[0] == getStackTrace
            // stack[1] == error
            val callingMethod = stack[stackFrameToPrint]
            errorLocationStr = String.format("MethodError = %s ", callingMethod.toString())
        }

        val messageToLog = errorLocationStr + message
        Timber.tag(CommonPlatformConstants.TAG).e(messageToLog, *args)
        if (!SystemUtils.isInstalledFromMarketPlace) {
            toast(messageToLog)
        }

    }

    @JvmStatic
    fun logFunc() {
        if (!SystemUtils.isInstalledFromMarketPlace) {
            // On Android there is an extra stack frame that gets the stack trace.
            val stackFrameToPrint = if (SystemUtils.isRunningOnAndroid) 3 else 2

            val stack = Thread.currentThread().stackTrace
            if (stack != null && stack.size > stackFrameToPrint + 1) {
                // stack[0] == getStackTrace
                // stack[1] == logFunc
                val callingMethod = stack[stackFrameToPrint]
                verbose("%s", callingMethod.toString())
            }
        }
    }

    @JvmStatic
    fun logFuncWithMessage(message: String, vararg args: Any) {
        var traceLocationStr = ""
        if (!SystemUtils.isInstalledFromMarketPlace) {
            // On Android there is an extra stack frame that gets the stack trace.
            val stackFrameToPrint = if (SystemUtils.isRunningOnAndroid) 3 else 2
            val stack = Thread.currentThread().stackTrace

            if (stack != null && stack.size > stackFrameToPrint + 1) {
                // stack[0] == getStackTrace
                // stack[1] == logFuncWithMessage
                val callingMethod = stack[stackFrameToPrint]
                traceLocationStr = String.format("%s", callingMethod.toString())
            }
        }
        verbose(traceLocationStr + message, *args)
    }

    @JvmStatic
    fun throwable(t: Throwable?, customMessageTemplate: String, vararg args: Any) {
        val customMessage = String.format(customMessageTemplate, *args)
        var message = "$customMessage Exception Message: "
        message += if (t != null) t.message else "Null Throwable"
        Timber.tag(CommonPlatformConstants.TAG).e(t)
    }

    @JvmStatic
    fun throwable(t: Throwable) {
        throwable(t, "")
    }

}
