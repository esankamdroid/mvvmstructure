package client.utils

import androidx.databinding.ObservableList
import client.base.BaseRecyclerViewItemViewModel
import client.base.BindingRecyclerViewAdapter
import java.lang.ref.WeakReference
import androidx.recyclerview.widget.RecyclerView
import android.icu.lang.UCharacter.GraphemeClusterBreak.T


class WeakReferenceOnListChangedCallback<T : BaseRecyclerViewItemViewModel>(bindingRecyclerViewAdapter: BindingRecyclerViewAdapter<T>) :
    ObservableList.OnListChangedCallback<ObservableList<T>>() {

    private var adapterReference: WeakReference<BindingRecyclerViewAdapter<T>>

    init {
        adapterReference = WeakReference<BindingRecyclerViewAdapter<T>>(bindingRecyclerViewAdapter)
    }

    override fun onChanged(sender: ObservableList<T>?) {
        val adapter = adapterReference.get()
        if (adapter != null) {
            adapter.notifyDataSetChanged()
        }
    }

    override fun onItemRangeRemoved(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
        val adapter = adapterReference.get()
        adapter?.notifyItemRangeRemoved(positionStart, itemCount)
    }

    override fun onItemRangeMoved(sender: ObservableList<T>?, fromPosition: Int, toPosition: Int, itemCount: Int) {
        val adapter = adapterReference.get()
        adapter?.notifyItemMoved(fromPosition, toPosition)
    }

    override fun onItemRangeInserted(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
        val adapter = adapterReference.get()
        adapter?.notifyItemRangeInserted(positionStart, itemCount)
    }

    override fun onItemRangeChanged(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
        val adapter = adapterReference.get()
        adapter?.notifyItemRangeChanged(positionStart, itemCount)
    }


}