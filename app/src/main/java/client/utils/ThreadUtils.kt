package client.utils


import android.os.Looper

object ThreadUtils {
    val isMainThread: Boolean
        get() = Looper.myLooper() == Looper.getMainLooper()

    fun verifyNotInMainThreadOnDevice(actionName: String) {
        if (SystemUtils.isRunningOnAndroid && ThreadUtils.isMainThread) {
            throw IllegalThreadStateException("$actionName Cannot be called on the main thread")
        }
    }

    fun verifyInMainThreadOnDevice(actionName: String) {
        if (SystemUtils.isRunningOnAndroid && !ThreadUtils.isMainThread) {
            throw IllegalThreadStateException("$actionName Can only be called on the main thread")
        }
    }
}
