package client.utils


object StringUtils {
    private fun isNullOrEmpty(s: String?): Boolean {
        return s == null || s.isEmpty() || s.equals("NaN", ignoreCase = true) || s.equals("null", ignoreCase = true)
    }

    @JvmStatic
    fun hasContent(s: String?): Boolean {
        return s != null && !isNullOrEmpty(s.trim { it <= ' ' })
    }

    fun trimStart(s: String, prefix: String): String {
        var s = s
        s = if (s.startsWith(prefix)) s.substring(prefix.length) else s
        return s
    }

    fun trimEnd(s: String, suffix: String): String {
        var s = s
        s = if (s.endsWith(suffix)) s.substring(0, s.length - suffix.length) else s
        return s
    }

    fun stringToBoolean(s: String): Boolean {
        return if (hasContent(s)) {
            s.equals("true", ignoreCase = true)
        } else false
    }

    object Constants {
        var UTF_8 = "UTF-8"
    }

    fun capitalized(string: String): String {
        return string.substring(0, 1).toUpperCase() + string.substring(1)
    }
}
