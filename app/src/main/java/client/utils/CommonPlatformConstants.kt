package client.utils

/**
 * Core Constants that are used across the application.
 */
object CommonPlatformConstants {

    val TAG = "App"
    val BASE_URL = ""

    val SHARED_PREFERENCE_NAME = "App_Preferences"
}
