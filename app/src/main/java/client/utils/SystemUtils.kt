package client.utils


import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager

object SystemUtils {

    private var sIsInstalledFromMarketPlace: Boolean = false
    private var sIsRunningOnAndroid: Boolean = false
    private var sInitialized = false

    val isInstalledFromMarketPlace: Boolean
        get() {
            if (!sInitialized) {
                throw IllegalStateException("System Utils have not been intialized")
            }
            return sIsInstalledFromMarketPlace
        }

    val isRunningOnAndroid: Boolean
        get() {
            if (!sInitialized) {
                throw IllegalStateException("System Utils have not been intialized")
            }
            return sIsRunningOnAndroid
        }

    val isRunningAsTest: Boolean
        get() {
            if (!sInitialized) {
                throw IllegalStateException("System Utils have not been intialized")
            }
            return !sIsRunningOnAndroid
        }

    fun initializeSystemUtils(installer: String) {

        if (!ThreadUtils.isMainThread) {
            throw IllegalThreadStateException("Needs to be called on main thread")
        }

        sIsInstalledFromMarketPlace = StringUtils.hasContent(installer)

        val androidVendor = System.getProperty("java.vendor")
        sIsRunningOnAndroid = androidVendor!!.contains("Android")

        sInitialized = true

    }

    fun openPlayStore(activity: Activity) {
        val appPackageName = activity.packageName // getPackageName() from Context or Activity object
        try {
            activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
        } catch (anfe: android.content.ActivityNotFoundException) {
            activity.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }

    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }

    fun pxToDp(px: Int): Int {
        return (px / Resources.getSystem().displayMetrics.density).toInt()
    }

    fun pxToDp(px: Float): Int {
        return (px / Resources.getSystem().displayMetrics.density).toInt()
    }

}
