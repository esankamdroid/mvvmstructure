package client.utils

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import client.interfaces.OnLoadMoreListener

class EndlessRecyclerOnScrollListener(
    layoutManager: RecyclerView.LayoutManager,
    onLoadMoreListener: OnLoadMoreListener
) : RecyclerView.OnScrollListener() {

    private var previousTotal = 0 // The total number of items in the dataset after the last load
    private var loading = true // True if we are still waiting for the last set of data to load.
    private val visibleThreshold =
        5 // The minimum amount of items to have below your current scroll position before loading more.
    private var firstVisibleItem: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0

    private var current_page = 1

    private var layoutManager: RecyclerView.LayoutManager? = layoutManager
    private var loadMoreListener: OnLoadMoreListener? = onLoadMoreListener

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        visibleItemCount = recyclerView.childCount
        totalItemCount = layoutManager!!.itemCount
        if (layoutManager is LinearLayoutManager)
            firstVisibleItem = (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        if (layoutManager is GridLayoutManager)
            firstVisibleItem = (layoutManager as GridLayoutManager).findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }

        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            // Do something
            current_page += 1;
            Trace.logFuncWithMessage("End Sucess %d", current_page)
            if (loadMoreListener != null)
                loadMoreListener!!.onLoadMore(current_page)

            loading = true;
        }
    }

}