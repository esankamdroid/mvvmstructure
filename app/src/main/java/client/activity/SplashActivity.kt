package client.activity

import client.base.BaseActivity
import client.interfaces.IViewModel
import com.client.android.R

class SplashActivity : BaseActivity() {

    override fun getView(): Int {
        return R.layout.activity_splash
    }

    override fun createViewModel(): IViewModel {
        return mViewModelProvider.createSplashViewModel(activityComponent)
    }

}
