package client.injection.components

import client.base.BaseActivity
import client.base.BaseDialogFragment
import client.base.BaseFragment
import client.injection.modules.*
import client.interfaces.IEventBus
import client.interfaces.IWebClient
import client.modules.dialog.IDialogManager
import client.modules.fcm.FCMMessagingService
import client.modules.fcm.IFCMManager
import client.modules.permission.IPermissionManager
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ApplicationModule::class,
        WebClientModule::class,
        EventBusModule::class,
        DialogManagerModule::class,
        PermissionManagerModule::class,
        FCMManagerModule::class
        /*DatabaseManagerModule::class*/
    ]
)
interface IPlatformComponent {

    fun getWebClient(): IWebClient

    fun getEventBus(): IEventBus

    fun getDialogManager(): IDialogManager

    fun getConfigurationManager(): IPermissionManager

    fun getFCMManager(): IFCMManager

    /*fun getDatabaseManager(): IDatabaseManager*/

    fun inject(baseActivity: BaseActivity)

    fun inject(baseFragment: BaseFragment)

    fun inject(baseFragment: BaseDialogFragment)

    fun inject(service: FCMMessagingService)

}
