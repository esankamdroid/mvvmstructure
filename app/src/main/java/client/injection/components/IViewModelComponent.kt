package client.injection.components


import client.injection.modules.ActivityViewModelModule
import client.injection.modules.ApplicationModule
import client.injection.scopes.ActivityScope
import client.viewmodel.SplashViewModel
import dagger.Component

@ActivityScope
@Component(
    dependencies = [IPlatformComponent::class],
    modules = [ActivityViewModelModule::class, ApplicationModule::class]
)
interface IViewModelComponent {

    fun inject(viewModel: SplashViewModel)

}
