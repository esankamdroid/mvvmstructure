package client.injection.modules

import client.interfaces.IModuleLifecycleManager
import client.modules.permission.PermissionManager
import client.modules.permission.IPermissionManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class PermissionManagerModule {

    @Singleton
    @Provides
    fun getConfigurationManager(component: PermissionManager, lifecycleManager: IModuleLifecycleManager): IPermissionManager {
        lifecycleManager.registerModule(component)
        return component
    }
}