package client.injection.modules

import client.base.ComponentFactory
import client.base.ModuleLifecycleManager
import client.factory.ViewModelFactory
import client.interfaces.*
import client.modules.preference.ISharedPreference
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(
    var application: IApp?,
    var mComponentFactory: ComponentFactory?,
    var mViewModelFactory: IViewModelFactory?,
    var mFragmentFactory: IFragmentFactory?,
    var mSharedPreference: ISharedPreference?,
    var mModuleLifecycleManager: ModuleLifecycleManager?
) {

    val applicationContext: android.content.Context
        @Provides
        get() = application!!.applicationContext

    val app: IApp
        @Provides
        get() = application!!

    val componentFactory: IComponentFactory
        @Provides
        get() = mComponentFactory!!

    val viewModelFactory: IViewModelFactory
        @Provides
        get() = mViewModelFactory!!

    val fragmentFactory: IFragmentFactory
        @Provides
        get() = mFragmentFactory!!

    val sharedPreference: ISharedPreference
        @Provides
        get() = mSharedPreference!!

    val moduleLifecycleManager: IModuleLifecycleManager
        @Provides
        get() = mModuleLifecycleManager!!

    fun cleanup() {
        mModuleLifecycleManager!!.cleanup()
        application = null
        mModuleLifecycleManager = null
        mSharedPreference = null
        mComponentFactory = null
        mFragmentFactory = null

    }
}