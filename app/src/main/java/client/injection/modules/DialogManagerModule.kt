package client.injection.modules

import client.interfaces.IModuleLifecycleManager
import client.modules.dialog.DialogManager
import client.modules.dialog.IDialogManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class DialogManagerModule {

    @Singleton
    @Provides
    fun getDialogManager(component: DialogManager, lifecycleManager: IModuleLifecycleManager): IDialogManager {
        lifecycleManager.registerModule(component)
        return component
    }
}