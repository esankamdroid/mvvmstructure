package client.injection.modules

import client.base.BaseActivity
import client.base.ViewModelToActivity
import client.interfaces.IViewModelToActivity
import client.injection.scopes.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class ActivityViewModelModule(activity: BaseActivity) {

    private val mModelToActivity: IViewModelToActivity

    init {
        this.mModelToActivity = getNewProxy(activity)
    }

    @Provides
    @ActivityScope
    fun provideActivityProxy(): IViewModelToActivity {
        return mModelToActivity
    }

    protected fun getNewProxy(activity: BaseActivity): IViewModelToActivity {
        return ViewModelToActivity(activity)
    }

    val viewModelToActivity: IViewModelToActivity
        get() = mModelToActivity
}

