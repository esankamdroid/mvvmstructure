package client.injection.modules

import client.base.EventBus
import client.interfaces.IEventBus
import client.webclient.WebClient
import client.interfaces.IModuleLifecycleManager
import client.interfaces.IWebClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class EventBusModule {

    @Singleton
    @Provides
    fun getEventBus(component: EventBus, lifecycleManager: IModuleLifecycleManager): IEventBus {
        lifecycleManager.registerModule(component)
        return component
    }
}