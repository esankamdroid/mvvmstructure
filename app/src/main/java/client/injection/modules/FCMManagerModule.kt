package client.injection.modules

import client.interfaces.IModuleLifecycleManager
import client.modules.fcm.FCMManager
import client.modules.fcm.IFCMManager
import dagger.Module
import dagger.Provides

import javax.inject.Singleton

@Singleton
@Module
class FCMManagerModule {

    @Singleton
    @Provides
    fun getFCMManager(component: FCMManager, lifecycleManager: IModuleLifecycleManager): IFCMManager {
        lifecycleManager.registerModule(component)
        return component
    }
}
