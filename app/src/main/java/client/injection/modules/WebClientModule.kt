package client.injection.modules

import client.webclient.WebClient
import client.interfaces.IModuleLifecycleManager
import client.interfaces.IWebClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class WebClientModule {

    @Singleton
    @Provides
    fun getWebClient(component: WebClient, lifecycleManager: IModuleLifecycleManager): IWebClient {
        lifecycleManager.registerModule(component)
        return component
    }
}