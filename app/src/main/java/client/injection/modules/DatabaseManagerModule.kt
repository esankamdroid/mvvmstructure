package client.injection.modules

import client.interfaces.IModuleLifecycleManager
import client.modules.room.DatabaseManager
import client.modules.room.IDatabaseManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class DatabaseManagerModule {

    @Singleton
    @Provides
    fun getDatabaseManager(component: DatabaseManager, lifecycleManager: IModuleLifecycleManager): IDatabaseManager {
        lifecycleManager.registerModule(component)
        return component
    }
}
