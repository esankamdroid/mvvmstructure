package client.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import client.injection.components.IViewModelComponent
import client.interfaces.IApp
import client.interfaces.IViewModelFactory
import client.viewmodel.SplashViewModel

class ViewModelFactory(val application: IApp) : IViewModelFactory {

    private inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>): T = f() as T
        }

    override fun createSplashViewModel(component: IViewModelComponent): SplashViewModel {
        return ViewModelProviders.of(
            application.activity,
            viewModelFactory { SplashViewModel(component) }
        ).get(SplashViewModel::class.java)
    }

}
