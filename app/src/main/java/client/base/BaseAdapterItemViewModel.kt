package client.base

import client.injection.components.IViewModelComponent

abstract class BaseAdapterItemViewModel constructor(activityComponent: IViewModelComponent) {

    abstract val view: Int

    abstract val dropView: Int

    abstract val bindingVariable: Int

    init {
        this.injectMembers(activityComponent)
    }

    abstract fun injectMembers(activityComponent: IViewModelComponent)

}