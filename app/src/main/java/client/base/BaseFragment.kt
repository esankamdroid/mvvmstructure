package client.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import client.App
import client.injection.components.IViewModelComponent
import client.interfaces.IViewModel
import client.interfaces.IViewModelFactory
import com.client.android.BR
import javax.inject.Inject

abstract class BaseFragment : Fragment() {

    private var viewModel: IViewModel? = null

    @Inject
    lateinit var mViewModelProvider: IViewModelFactory

    @LayoutRes
    abstract fun getLayout(): Int

    abstract fun createViewModel(activityComponent: IViewModelComponent): IViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val activity = activity as BaseActivity
        viewModel = createViewModel(activity.activityComponent)
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(inflater, getLayout(), container, false)
        binding.setVariable(BR.viewModel, viewModel)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val application = App.instance
        val platformLifetimeComponent = application.platformLayer.applicationComponent
        platformLifetimeComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewModel != null && arguments != null) {
            viewModel!!.getDataFromBundle(arguments!!)
        }
    }

    override fun onStart() {
        super.onStart()
        if (viewModel != null)
            viewModel!!.onStart()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel != null)
            viewModel!!.onResume()
    }

    override fun onPause() {
        super.onPause()

        if (viewModel != null)
            viewModel!!.onPause()
    }

    override fun onStop() {
        super.onStop()
        if (viewModel != null)
            viewModel!!.onStop()

    }

    override fun onDestroy() {
        super.onDestroy()
        if (viewModel != null)
            viewModel!!.onDestroy()
    }
}
