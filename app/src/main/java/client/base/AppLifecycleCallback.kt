package client.base

import android.app.Activity
import android.os.Bundle
import client.interfaces.IAppLifecycleCallback

class AppLifecycleCallback : IAppLifecycleCallback {

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {

    }

    private var activity: Activity? = null

    override fun onActivityCreated(activity: Activity?, bundle: Bundle?) {
        this.activity = activity
    }

    override fun onActivityStarted(activity: Activity?) {
        this.activity = activity
    }

    override fun onActivityResumed(activity: Activity?) {
        this.activity = activity
    }

    override fun onActivityPaused(activity: Activity?) {}

    override fun onActivityStopped(activity: Activity?) {

    }

    override fun onActivityDestroyed(activity: Activity?) {

    }

    override fun getActivity(): Activity {
        return activity!!
    }
}