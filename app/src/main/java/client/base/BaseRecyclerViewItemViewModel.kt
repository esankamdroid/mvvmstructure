package client.base

import client.injection.components.IViewModelComponent
import javax.inject.Inject

abstract class BaseRecyclerViewItemViewModel constructor(activityComponent: IViewModelComponent) {

    @Inject
    protected lateinit var mAttachedActivity: IViewModelComponent

    abstract val layoutId: Int
    abstract val bindingVariable: Int

    init {
        this.injectMembers(activityComponent)
    }

    protected abstract fun injectMembers(activityComponent: IViewModelComponent)

}