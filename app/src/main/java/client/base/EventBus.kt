package client.base

import client.interfaces.IBaseEvent
import client.interfaces.IEventBus
import client.interfaces.IEventBusListener
import client.utils.Trace
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

import javax.inject.Inject

/**
 * Implementation of IEventBus.
 * Consumers can call send on any thread.
 * All events will be delivered on the main thread regardless of which thread they were dispatched
 * from
 */
class EventBus @Inject constructor() : IEventBus {

    private var _bus: Subject<IBaseEvent>? = null

    init {
        val rxEventPublishSubject = PublishSubject.create<IBaseEvent>()
        _bus = rxEventPublishSubject.toSerialized()
    }

    override fun send(event: IBaseEvent) {
        Trace.info("event fired of type = %s", event.javaClass.canonicalName!!)
        _bus!!.onNext(event)
    }

    override fun subscribe(eventBusListener: IEventBusListener): Disposable {
        Trace.info("Subscriber of type = %s", eventBusListener.javaClass.canonicalName!!)
        return toObservable()!!.observeOn(AndroidSchedulers.mainThread())
            .subscribe { iBaseEvent -> eventBusListener.onEvent(iBaseEvent) }
    }

    private fun toObservable(): Observable<IBaseEvent>? {
        return _bus
    }

    fun cleanup() {
        Trace.logFunc()
        _bus!!.onComplete()
        _bus = null
    }

    override fun cleanupModule() {
        cleanup()
    }
}
