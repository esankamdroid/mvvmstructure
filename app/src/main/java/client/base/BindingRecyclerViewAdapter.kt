package client.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import client.interfaces.IViewClickListener
import client.utils.WeakReferenceOnListChangedCallback

class BindingRecyclerViewAdapter<T : BaseRecyclerViewItemViewModel>(items: Collection<T>?) :
    RecyclerView.Adapter<BindingRecyclerViewAdapter.ViewHolder>(), View.OnClickListener {

    private val onListChangedCallback: WeakReferenceOnListChangedCallback<T>
    var items: ObservableList<T>? = null
        private set
    private var inflater: LayoutInflater? = null
    private var clickHandler: IViewClickListener<T>? = null

    init {
        this.onListChangedCallback = WeakReferenceOnListChangedCallback(this)
        setItems(items)
    }

    fun setItems(items: Collection<T>?) {
        if (this.items === items) {
            return
        }

            if (this.items != null) {
                this.items!!.removeOnListChangedCallback(onListChangedCallback)
                notifyItemRangeRemoved(0, this.items!!.size)
            }

        if (items is ObservableList<*>) {
            this.items = items as ObservableList<T>?
            notifyItemRangeInserted(0, this.items!!.size)
            this.items!!.addOnListChangedCallback(onListChangedCallback)
        } else if (items != null) {
            this.items = ObservableArrayList()
            this.items!!.addOnListChangedCallback(onListChangedCallback)
            this.items!!.addAll(items)
        } else {
            this.items = null
        }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        if (items != null) {
            items!!.removeOnListChangedCallback(onListChangedCallback)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, layoutId: Int): ViewHolder {
        if (inflater == null) {
            inflater = LayoutInflater.from(viewGroup.context)
        }

        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater!!, layoutId, viewGroup, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = items!![position]
        viewHolder.binding.setVariable(item.bindingVariable, item) // set variable name that are needed by the view
        viewHolder.binding.root.setTag(ITEM_MODEL, item)
        viewHolder.binding.root.setOnClickListener(this)
        viewHolder.binding.executePendingBindings()
    }

    override fun getItemViewType(position: Int): Int {
        return items!![position].layoutId
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    override fun onClick(v: View) {
        if (clickHandler != null) {
            val item = v.getTag(ITEM_MODEL) as T
            clickHandler!!.onClick(item)
        }
    }

    class ViewHolder constructor(internal val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)

    fun setClickHandler(clickHandler: IViewClickListener<T>) {
        this.clickHandler = clickHandler
    }

    companion object {
        private val ITEM_MODEL = -124
    }

}
