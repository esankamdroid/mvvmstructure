package client.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import client.widgets.expandablerecyclerview.Child;
import client.widgets.expandablerecyclerview.ChildrenViewHolder;
import client.widgets.expandablerecyclerview.Header;
import client.widgets.expandablerecyclerview.HeaderViewHolder;
import client.widgets.expandablerecyclerview.base.ExpandableRecyclerAdapter;
import com.client.android.R;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.List;

public class ExpandableAdapter extends ExpandableRecyclerAdapter<Header, Child, HeaderViewHolder, ChildrenViewHolder> {

    private static final int HEADER = 1011;
    private static final int CHILD = 1012;
    private ObservableList<Header> mHeaders;
    private LayoutInflater mInflater;
    private final WeakReferenceOnListChangedCallback onListChangedCallback;
    private int parentLayout;
    private int childLayout;

    public ExpandableAdapter(Context context, @NonNull Collection<Header> parentList, @LayoutRes int parentLayout, @LayoutRes int childLayout) {
        super((List<Header>) parentList);
        mInflater = LayoutInflater.from(context);
        this.parentLayout = parentLayout;
        this.childLayout = childLayout;
        onListChangedCallback = new WeakReferenceOnListChangedCallback(this);
        setItems(parentList);
    }

    public void setItems(@Nullable Collection<Header> items) {
        if (this.mHeaders == items) {
            return;
        }

        if (this.mHeaders != null) {
            this.mHeaders.removeOnListChangedCallback(onListChangedCallback);
            notifyItemRangeRemoved(0, this.mHeaders.size());
        }

        if (items instanceof ObservableList) {
            this.mHeaders = (ObservableList<Header>) items;
            notifyItemRangeInserted(0, this.mHeaders.size());
            this.mHeaders.addOnListChangedCallback(onListChangedCallback);
        } else if (items != null) {
            this.mHeaders = new ObservableArrayList<>();
            this.mHeaders.addOnListChangedCallback(onListChangedCallback);
            this.mHeaders.addAll(items);
        } else {
            this.mHeaders = null;
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        if (mHeaders != null) {
            mHeaders.removeOnListChangedCallback(onListChangedCallback);
        }
    }

    @NonNull
    @Override
    public HeaderViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        final ViewDataBinding binding = DataBindingUtil.inflate(mInflater, parentLayout, parentViewGroup, false);
        return new HeaderViewHolder(binding);
    }

    @NonNull
    @Override
    public ChildrenViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        final ViewDataBinding binding = DataBindingUtil.inflate(mInflater, childLayout, childViewGroup, false);
        return new ChildrenViewHolder(binding);
    }

    @Override
    public void onBindParentViewHolder(@NonNull HeaderViewHolder viewHolder, int parentPosition, @NonNull Header item) {
        item.setViewHolder(viewHolder);
        viewHolder.binding.setVariable(item.getBindingVariable(), item);
        viewHolder.binding.getRoot().setTag(R.string.header, item);
        viewHolder.binding.executePendingBindings();
    }

    @Override
    public void onBindChildViewHolder(@NonNull ChildrenViewHolder viewHolder, int parentPosition, int childPosition, @NonNull Child item) {
        viewHolder.binding.setVariable(item.getBindingVariable(), item);
        viewHolder.binding.getRoot().setTag(R.string.child, item);
        viewHolder.binding.executePendingBindings();
    }


    @Override
    public int getParentViewType(int parentPosition) {
        return mHeaders.get(parentPosition).isParent() ? HEADER : CHILD;
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        return mHeaders.get(parentPosition).getChildList().get(childPosition).isChild() ? CHILD : HEADER;
    }

    @Override
    public boolean isParentViewType(int viewType) {
        return viewType == HEADER;
    }

    private static class WeakReferenceOnListChangedCallback extends ObservableList.OnListChangedCallback {

        private final WeakReference<ExpandableRecyclerAdapter> adapterReference;

        WeakReferenceOnListChangedCallback(ExpandableRecyclerAdapter bindingRecyclerViewAdapter) {
            this.adapterReference = new WeakReference<>(bindingRecyclerViewAdapter);
        }

        @Override
        public void onChanged(ObservableList sender) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onItemRangeChanged(ObservableList sender, int positionStart, int itemCount) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyItemRangeChanged(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeInserted(ObservableList sender, int positionStart, int itemCount) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyItemRangeInserted(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeMoved(ObservableList sender, int fromPosition, int toPosition, int itemCount) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyItemMoved(fromPosition, toPosition);
            }
        }

        @Override
        public void onItemRangeRemoved(ObservableList sender, int positionStart, int itemCount) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyItemRangeRemoved(positionStart, itemCount);
            }
        }
    }
}
