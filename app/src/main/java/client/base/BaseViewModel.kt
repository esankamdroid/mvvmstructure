package client.base

import android.os.Bundle
import androidx.lifecycle.ViewModel
import client.injection.components.IViewModelComponent
import client.interfaces.*
import client.modules.permission.event.IPermissionManagerEvent
import client.utils.Trace
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


abstract class BaseViewModel(activityComponent: IViewModelComponent) : ViewModel(),
    IViewModel, IEventBusListener {

    @Inject
    protected lateinit var mEventBus: IEventBus
    @Inject
    protected lateinit var mApp: IApp
    @Inject
    protected lateinit var mViewModelToActivity: IViewModelToActivity

    protected lateinit var mViewModelFactory: IViewModelFactory

    protected var mSubscription: CompositeDisposable

    abstract override fun getDataFromBundle(bundle: Bundle)

    abstract fun injectMember(activityComponent: IViewModelComponent)

    init {
        Trace.logFuncWithMessage("view model %s created", javaClass.simpleName)
        this.injectMember(activityComponent)
        mSubscription = CompositeDisposable()
        mSubscription.add(mEventBus.subscribe(this))

        createChildViewModel(activityComponent)
    }

    fun createChildViewModel(activityComponent: IViewModelComponent) {

    }

    override fun onStart() {
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onStop() {
    }

    override fun onDestroy() {
        Trace.logFuncWithMessage("view model %s destroyed", javaClass.simpleName)
        mSubscription.clear()
        mSubscription.dispose()
    }

    override fun onEvent(baseEvent: IBaseEvent) {
        Trace.logFunc()
        if (baseEvent.isEventOfPassedType(IPermissionManagerEvent::class.java)) run {
            val event = baseEvent as IPermissionManagerEvent
            val eventType = event.eventType
            if (eventType == IPermissionManagerEvent.EventType.ShowPermissionRationale) {
                Trace.logFuncWithMessage("ask permission rationale")
                mViewModelToActivity.showAppDetailSetting()
            }
        }
    }
}