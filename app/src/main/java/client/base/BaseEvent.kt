package client.base

import client.interfaces.IBaseEvent

abstract class BaseEvent : IBaseEvent {

    override fun isEventOfPassedType(eventType: Class<*>): Boolean {
        return eventType.isInstance(this)
    }
}