package client.base

import client.interfaces.IBaseModule
import client.interfaces.IModuleLifecycleManager
import client.utils.Trace
import javax.inject.Inject

class ModuleLifecycleManager @Inject constructor() : IModuleLifecycleManager {

    private var baseModules: ArrayList<IBaseModule>? = null

    init {
        baseModules = ArrayList()
    }

    override fun registerModule(module: IBaseModule) {
        if (!baseModules!!.contains(module)) {
            Trace.logFuncWithMessage("registered module name = %s", module.javaClass.canonicalName!!)
            baseModules!!.add(module)
        }
    }

    override fun resetAndRemoveModules() {
        for (m in baseModules!!) {
            Trace.info("cleaning up module = %s", m.javaClass.canonicalName!!)
            m.cleanupModule()
        }
        baseModules!!.clear()
    }

    fun cleanup() {
        baseModules = null
    }
}
