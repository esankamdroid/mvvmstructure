package client.base

import client.injection.components.DaggerIViewModelComponent
import client.injection.components.IViewModelComponent
import client.injection.modules.ActivityViewModelModule
import client.interfaces.IApp
import client.interfaces.IComponentFactory


class ComponentFactory(private val mApplication: IApp) : IComponentFactory {

    override fun createActivityViewModelModule(module: ActivityViewModelModule): IViewModelComponent {
        return DaggerIViewModelComponent.builder()
            .applicationModule(mApplication.applicationModule)
            .iPlatformComponent(mApplication.platformLayer.applicationComponent)
            .activityViewModelModule(module)
            .build()
    }
}
