package client.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

class BaseArrayAdapter<T : BaseAdapterItemViewModel>(context: Context, val items: Collection<T>) :
    ArrayAdapter<T>(context, 0) {

    private val inflater = LayoutInflater.from(context);

    init {
        addAll(items)
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item = getItem(position)!!
        var view = convertView
        val viewHolder: DropViewHolder
        if (view == null) {
            val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, item.dropView, parent, false)
            viewHolder = DropViewHolder(binding!!)
            view = binding.root
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as BaseArrayAdapter<T>.DropViewHolder
        }
        viewHolder.binding.setVariable(item.bindingVariable, item)
        viewHolder.binding.executePendingBindings()
        return view
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item = getItem(position)!!
        var view = convertView
        val viewHolder: ViewHolder
        if (view == null) {
            val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, item.view, parent, false)
            viewHolder = ViewHolder(binding!!)
            view = binding.root
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as BaseArrayAdapter<T>.ViewHolder
        }
        viewHolder.binding.setVariable(item.bindingVariable, item)
        viewHolder.binding.executePendingBindings()
        return view
    }

    inner class ViewHolder(val binding: ViewDataBinding)
    inner class DropViewHolder(val binding: ViewDataBinding)
}