package client.base

import androidx.annotation.CallSuper
import client.App
import client.injection.components.ITaskComponent
import client.interfaces.IWebClient
import client.interfaces.IWebTaskCallback
import client.modules.dialog.IDialogManager
import client.utils.Trace
import client.webclient.ResultResponseTransformer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject

abstract class BaseWebTask<T, E>(val mWebTaskCallback: IWebTaskCallback<T>) : WebTask() {

    private var mApiCall: Observable<Result<T>>? = null
    private var mOnSuccessfulApiResponse: Consumer<Response<T>>? = null
    private val mCompositeSubscription: CompositeDisposable
    private var mTaskComponent: ITaskComponent
    protected var endPoint: E
    abstract fun getEndPoint(): Class<E>

    @Inject
    lateinit var webClient: IWebClient

    @Inject
    lateinit var mDialogManager: IDialogManager

    val taskComponent: ITaskComponent
        get() = mTaskComponent

    init {
        mTaskComponent = App.instance.platformLayer.taskComponent
        this.injectTask()
        endPoint = createEndPoint(this.getEndPoint())
        mCompositeSubscription = CompositeDisposable()
        mDialogManager.show()
        this.doTaskOperation()
    }

    private fun createEndPoint(endPoint: Class<E>): E {
        return webClient.createWebService(endPoint)
    }

    @CallSuper
    fun cleanUp() {
        Trace.logFunc()
        mApiCall = null
        mOnSuccessfulApiResponse = null
        mCompositeSubscription.clear()
        mCompositeSubscription.dispose()
        mDialogManager.dismiss()
    }


    fun cancel() {
        Trace.logFunc()
        this.cleanUp()
    }

    fun startWebTask(
        apiCall: Observable<Result<T>>,
        onSuccessfulApiResponse: Consumer<Response<T>>
    ) {
        mApiCall = apiCall
        mOnSuccessfulApiResponse = onSuccessfulApiResponse
        startWebCall()
    }

    private fun startWebCall() {
        val onSuccessfulApiResponse = this.mOnSuccessfulApiResponse
        mCompositeSubscription.add(
            this.mApiCall!!.compose(ResultResponseTransformer())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onSuccessfulApiResponse!!.accept(it)
                    },
                    {
                        onApiError(it)
                    },
                    {
                        onWebTaskCompleted()
                    }
                )
        )
    }

    private fun onApiError(t: Throwable) {
        Trace.throwable(t)
        val failure = t.message
        mWebTaskCallback.onTaskFailed(failure!!)
        cleanUp()
    }

    protected fun onFailureApiResponse(response: Response<T>) {
        if (!response.isSuccessful) {
            val message = response.message()
            Trace.error(message)
            mWebTaskCallback.onTaskFailed(message)
        }
    }

    private fun onWebTaskCompleted() {
        Trace.logFunc()
        cleanUp()
    }

    fun dispatchResultToCallbackAndCleanup(apiResponse: Response<T>) {

        if (!apiResponse.isSuccessful || apiResponse.body() == null) {
            onFailureApiResponse(apiResponse)
        } else {
            val responseBody = apiResponse.body()
            mWebTaskCallback.onTaskComplete(responseBody!!)
        }
    }
}
