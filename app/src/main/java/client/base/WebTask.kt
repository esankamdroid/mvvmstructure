package client.base

abstract class WebTask() {

    abstract fun doTaskOperation()

    abstract fun injectTask()

}
