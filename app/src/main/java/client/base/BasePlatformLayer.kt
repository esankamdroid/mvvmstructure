package client.base

import client.interfaces.IPlatformLayer
import client.injection.components.DaggerIPlatformComponent
import client.injection.components.DaggerITaskComponent
import client.injection.components.IPlatformComponent
import client.injection.components.ITaskComponent
import client.injection.modules.ApplicationModule
import client.utils.Trace

/**
 * Core service class. This is the root node of all the service components.
 * It is responsible for starting all the service components.
 */
class BasePlatformLayer(private val mApplicationModule: ApplicationModule) : IPlatformLayer {

    private var applicationLifetimeComponent: IPlatformComponent? = null
    private var mTaskComponent: ITaskComponent? = null

    init {
        Trace.logFunc()
        instance = this
        applicationLifetimeComponent = DaggerIPlatformComponent.builder()
            .applicationModule(mApplicationModule)
            .build()
        mTaskComponent = DaggerITaskComponent.builder()
            .iPlatformComponent(applicationLifetimeComponent)
            .build()

    }

    override val applicationComponent: IPlatformComponent
        get() = applicationLifetimeComponent!!

    override fun cleanup() {
        mApplicationModule.moduleLifecycleManager.resetAndRemoveModules()
        applicationLifetimeComponent = null
        instance = null
    }

    override val taskComponent: ITaskComponent
        get() = mTaskComponent!!

    companion object {

        //region member vars
        var instance: BasePlatformLayer? = null
            private set
    }
}