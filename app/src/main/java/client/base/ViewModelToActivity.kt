package client.base

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import client.interfaces.IViewModelToActivity
import client.utils.Trace
import java.lang.ref.WeakReference

class ViewModelToActivity(activity: BaseActivity) : IViewModelToActivity {


    private val weakActivity: WeakReference<BaseActivity>

    override val activity: AppCompatActivity
        get() = weakActivity.get()!!

    override val fragmentManager: FragmentManager?
        get() {
            val activity = weakActivity.get()
            return if (activity != null && !activity.isFinishing && activity.supportFragmentManager != null) {
                activity.supportFragmentManager
            } else null
        }

    init {
        weakActivity = WeakReference(activity)
    }

    override fun startActivity(activityClass: Class<out Activity>) {
        Trace.logFuncWithMessage("activity to start = %s", activityClass.canonicalName!!)
        val activity = weakActivity.get()
        if (activity != null && !activity.isFinishing) {
            activity.startActivity(Intent(activity, activityClass))
        }
    }

    override fun startActivityWithBundle(activityClass: Class<out Activity>, bundle: Bundle) {
        Trace.logFuncWithMessage("activity to start = %s", activityClass.canonicalName!!)
        val activity = weakActivity.get()
        if (activity != null && !activity.isFinishing) {
            val mIntent = Intent(activity, activityClass)
            mIntent.putExtras(bundle)
            activity.startActivity(mIntent)
        }
    }

    override fun startActivityAndFinishThis(activityClass: Class<out Activity>) {
        Trace.logFuncWithMessage("activity to start = %s", activityClass.canonicalName!!)
        val activity = weakActivity.get()
        if (activity != null && !activity.isFinishing) {
            val intent = Intent(activity, activityClass)
            activity.startActivity(Intent(activity, activityClass))
            activity.finish()
        }
    }

    override fun startActivityAndFinishThis(intentToStart: Intent) {
        val activity = weakActivity.get()
        if (activity != null && !activity.isFinishing) {
            activity.startActivity(intentToStart)
            activity.finish()
        }
    }

    override fun finish() {
        val activity = weakActivity.get()
        if (activity != null && !activity.isFinishing) {
            activity.finish()
        }
    }

    override fun showToast(message: String, duration: Int) {
        val activity = weakActivity.get()
        if (activity != null && !activity.isFinishing) {
            Toast.makeText(activity, message, duration).show()
        }
    }

    override fun setSupportActionToolbar(toolbar: Toolbar) {
        val activity = weakActivity.get()
        if (activity != null && !activity.isFinishing) {
            activity.setSupportActionBar(toolbar)
        }
    }

    override fun getString(stringIdFromResources: Int): String? {
        val activity = weakActivity.get()
        return if (activity != null && !activity.isFinishing) {
            activity.resources.getString(stringIdFromResources)
        } else null
    }

    override fun getString(stringIdFromResources: Int, vararg formatArgs: Any): String? {
        val activity = weakActivity.get()
        return if (activity != null && !activity.isFinishing) {
            activity.resources.getString(stringIdFromResources, *formatArgs)
        } else null
    }

    override fun getColor(colorIdFromResources: Int): Int {
        val activity = weakActivity.get()
        return if (activity != null && !activity.isFinishing) {
            activity.resources.getColor(colorIdFromResources)
        } else 0
    }

    override fun getDrawable(@DrawableRes id: Int): Drawable {
        return ContextCompat.getDrawable(weakActivity.get()!!, id)!!
    }

    override fun showAppDetailSetting() {
        if (activity != null && !activity.isFinishing) {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", activity.packageName, null)
            intent.data = uri;
            activity.startActivity(intent)
        }
    }
}

