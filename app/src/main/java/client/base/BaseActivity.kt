package client.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import client.App
import client.injection.components.IViewModelComponent
import client.injection.modules.ActivityViewModelModule
import client.interfaces.*
import client.modules.permission.IPermissionManager
import client.utils.Trace
import com.client.android.BR
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


abstract class BaseActivity : AppCompatActivity(), IEventBusListener {

    private var viewModel: IViewModel? = null
    private var mActivityComponent: IViewModelComponent? = null
    protected lateinit var mSubscription: CompositeDisposable

    @Inject
    lateinit var mViewModelProvider: IViewModelFactory
    @Inject
    protected lateinit var mEventBus: IEventBus
    @Inject
    protected lateinit var mApp: IApp
    @Inject
    protected lateinit var mPermissionManager: IPermissionManager

    @LayoutRes
    abstract fun getView(): Int

    abstract fun createViewModel(): IViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val application = application as App
        val platformLifetimeComponent = application.platformLayer.applicationComponent

        mActivityComponent = application
            .applicationModule
            .componentFactory
            .createActivityViewModelModule(ActivityViewModelModule(this))
        platformLifetimeComponent.inject(this)

        mSubscription = CompositeDisposable()

        viewModel = createViewModel()
        val binding = DataBindingUtil.setContentView<ViewDataBinding>(this, getView())
        binding.setVariable(BR.viewModel, viewModel)
        binding.lifecycleOwner = this

        Trace.logFuncWithMessage("activity %s created", javaClass.simpleName)
        if (intent.extras != null) {
            viewModel!!.getDataFromBundle(intent.extras!!)
        }
    }

    val activityComponent: IViewModelComponent
        get() = mActivityComponent!!

    override fun onStart() {
        super.onStart()
        if (viewModel != null)
            viewModel!!.onStart()
    }

    override fun onResume() {
        super.onResume()
        mSubscription.add(mEventBus.subscribe(this))
        if (viewModel != null)
            viewModel!!.onResume()
    }

    override fun onPause() {
        super.onPause()

        mSubscription.clear()
        if (viewModel != null)
            viewModel!!.onPause()
    }

    override fun onStop() {
        super.onStop()
        if (viewModel != null)
            viewModel!!.onStop()

    }

    override fun onDestroy() {
        super.onDestroy()
        Trace.logFuncWithMessage("activity %s destroyed", javaClass.simpleName)
        if (viewModel != null)
            viewModel!!.onDestroy()
        mSubscription.clear()
        mSubscription.dispose()
    }

    override fun onEvent(baseEvent: IBaseEvent) {

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mPermissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}