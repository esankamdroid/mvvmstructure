package client

import android.content.Context
import android.content.ContextWrapper
import androidx.appcompat.app.AppCompatActivity
import androidx.multidex.MultiDexApplication
import client.base.AppLifecycleCallback
import client.base.BasePlatformLayer
import client.base.ComponentFactory
import client.base.ModuleLifecycleManager
import client.factory.FragmentFactory
import client.factory.ViewModelFactory
import client.injection.modules.ApplicationModule
import client.interfaces.IApp
import client.interfaces.IAppLifecycleCallback
import client.interfaces.IPlatformLayer
import client.modules.preference.SharedPreference
import client.utils.StringUtils
import client.utils.SystemUtils
import client.utils.Trace
import timber.log.Timber


class App : MultiDexApplication(), IApp {

    private var mApplicationModule: ApplicationModule? = null
    private var mBasePlatformLayer: BasePlatformLayer? = null
    private lateinit var mLifecycleCallbacks: IAppLifecycleCallback

    companion object {

        private lateinit var mInstance: IApp

        @JvmStatic
        val instance: IApp
            get() = mInstance

    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        initializeApplicationEssentials(this)
        Trace.warn("Application created")
    }

    override fun onLowMemory() {
        super.onLowMemory()
        Trace.warn("low memory")
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Trace.warn("on Trim Memory = %d", level)
    }

    override fun onTerminate() {
        super.onTerminate()
        resetApp()
    }

    private fun resetApp() {
        if (mBasePlatformLayer != null) {
            mBasePlatformLayer!!.cleanup()
            mBasePlatformLayer = null
        }
        if (mApplicationModule != null) {
            mApplicationModule!!.cleanup()
            mApplicationModule = null
        }

        Trace.warn("Application terminate")
        Timber.uprootAll()
    }

    override fun initializeApplicationEssentials(c: Context) {
        val installerName = c.packageManager.getInstallerPackageName(c.packageName)
        SystemUtils.initializeSystemUtils(if (StringUtils.hasContent(installerName)) installerName else "")

        mLifecycleCallbacks = AppLifecycleCallback()
        registerActivityLifecycleCallbacks(mLifecycleCallbacks)

        if (!SystemUtils.isInstalledFromMarketPlace) {
            Timber.plant(Timber.DebugTree())
        }

        mApplicationModule = ApplicationModule(
            this,
            ComponentFactory(this),
            ViewModelFactory(this),
            FragmentFactory(),
            SharedPreference(this),
            ModuleLifecycleManager()
        )
        mBasePlatformLayer = BasePlatformLayer(mApplicationModule!!)
    }

    override val applicationContext: ContextWrapper
        get() = this

    override val applicationModule: ApplicationModule
        get() = mApplicationModule!!

    override val platformLayer: IPlatformLayer
        get() = mBasePlatformLayer!!

    override val activity: AppCompatActivity
        get() = mLifecycleCallbacks.getActivity() as AppCompatActivity

}