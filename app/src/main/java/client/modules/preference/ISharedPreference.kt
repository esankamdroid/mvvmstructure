package client.modules.preference

interface ISharedPreference {

    companion object {
        val TITLE = "Title"
        val TEST = "Test"
    }

    fun getBooleanValue(key: String, defaultValue: Boolean): Boolean

    fun setBooleanValue(key: String, value: Boolean)

    fun setStringValue(key: String, value: String)

    fun getStringValue(key: String, defaultValue: String): String

    fun setIntegerValue(key: String, value: Int)

    fun getIntegerValue(key: String, defaultValue: Int): Int

    fun setFloatValue(key: String, value: Float)

    fun getFloatValue(key: String, defaultValue: Float): Float

}
