package client.modules.preference

import android.content.Context
import android.content.SharedPreferences
import client.interfaces.IApp
import client.utils.CommonPlatformConstants

class SharedPreference(app: IApp) : ISharedPreference {

    private val mSharedPreferences: SharedPreferences

    init {
        mSharedPreferences = app.applicationContext.getSharedPreferences(
            CommonPlatformConstants.SHARED_PREFERENCE_NAME,
            Context.MODE_PRIVATE
        )
    }

    override fun getBooleanValue(key: String, defaultValue: Boolean): Boolean {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    override fun setBooleanValue(key: String, value: Boolean) {
        val editor = mSharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    override fun setStringValue(key: String, value: String) {
        val editor = mSharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    override fun getStringValue(key: String, defaultValue: String): String {
        return mSharedPreferences.getString(key, defaultValue)!!
    }

    override fun setIntegerValue(key: String, value: Int) {
        val editor = mSharedPreferences.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    override fun getIntegerValue(key: String, defaultValue: Int): Int {
        return mSharedPreferences.getInt(key, defaultValue)
    }

    override fun setFloatValue(key: String, value: Float) {
        val editor = mSharedPreferences.edit()
        editor.putFloat(key, value)
        editor.apply()
    }

    override fun getFloatValue(key: String, defaultValue: Float): Float {
        return mSharedPreferences.getFloat(key, defaultValue)
    }

}
