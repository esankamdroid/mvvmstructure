package client.modules.fcm

import client.App
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import javax.inject.Inject

class FCMMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var mFCManager: IFCMManager

    override fun onCreate() {
        super.onCreate()
        App.instance.platformLayer.applicationComponent.inject(this)
    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        mFCManager.onRefreshedToken(token!!)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        mFCManager.onMessageReceived(remoteMessage!!)
    }
}
