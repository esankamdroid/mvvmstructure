package client.modules.fcm

import client.utils.Trace
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.RemoteMessage

import javax.inject.Inject

class FCMManager @Inject constructor() : IFCMManager {

    override fun initFCM() {
        Trace.logFunc()
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }

    override fun onRefreshedToken(token: String) {
        Trace.logFuncWithMessage("Token => %s", token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Trace.logFunc()
    }

    override fun cleanupModule() {

    }
}
