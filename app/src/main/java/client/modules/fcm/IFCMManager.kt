package client.modules.fcm

import client.interfaces.IBaseModule
import com.google.firebase.messaging.RemoteMessage

interface IFCMManager : IBaseModule {

    fun initFCM()

    fun onRefreshedToken(token: String)

    fun onMessageReceived(remoteMessage: RemoteMessage)
}
