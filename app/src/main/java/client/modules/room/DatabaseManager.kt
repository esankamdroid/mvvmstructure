package client.modules.room

import android.content.Context
import client.base.EventBus
import client.interfaces.IBaseEvent
import client.interfaces.IEventBusListener
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DatabaseManager @Inject constructor(context: Context, eventBus: EventBus) : IDatabaseManager, IEventBusListener {

    private val compositeDisposable = CompositeDisposable()

    companion object {

    }

    init {
        compositeDisposable.add(eventBus.subscribe(this))
    }

    override fun cleanupModule() {
        compositeDisposable.clear()
        compositeDisposable.dispose()
    }

    override fun onEvent(baseEvent: IBaseEvent) {

    }
}