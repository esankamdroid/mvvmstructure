package client.modules.permission

import client.interfaces.IBaseModule


interface IPermissionManager : IBaseModule {

    enum class Permission {
        WRITE_EXTERNAL_STORAGE
    }

    fun isPermissionGranted(permission: Permission): Boolean

    fun requestPermission(settingToRequest: Permission)

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
}
