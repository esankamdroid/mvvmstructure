package client.modules.permission

import android.Manifest
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import client.interfaces.IApp
import client.interfaces.IBaseEvent
import client.interfaces.IEventBus
import client.interfaces.IEventBusListener
import client.modules.permission.event.IPermissionManagerEvent
import client.modules.permission.event.PermissionManagerEvent
import client.utils.Trace
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Inject


class PermissionManager @Inject constructor(private val mEventBus: IEventBus, private val mApp: IApp) :
    IPermissionManager, IEventBusListener {
    private val disposable: CompositeDisposable
    private val permissionMap: HashMap<IPermissionManager.Permission, PermissionModel>?

    init {
        disposable = CompositeDisposable()
        permissionMap = HashMap<IPermissionManager.Permission, PermissionModel>()
        disposable.add(mEventBus.subscribe(this))

        addPermission(
            PermissionModel(
                IPermissionManager.Permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, "Please allow to read you internal storage"
            )
        )
    }


    override fun cleanupModule() {
        permissionMap!!.clear()
        disposable.clear()
        disposable.dispose()
    }

    private fun addPermission(model: PermissionModel) {
        if (!permissionMap!!.containsKey(model.permission))
            permissionMap[model.permission] = model
    }

    override fun isPermissionGranted(permission: IPermissionManager.Permission): Boolean {
        return if (permissionMap != null && permissionMap[permission] != null) {
            ContextCompat.checkSelfPermission(
                mApp.activity,
                permissionMap[permission]!!.name
            ) == PackageManager.PERMISSION_GRANTED
        } else false
    }

    override fun requestPermission(permission: IPermissionManager.Permission) {
        if (!isPermissionGranted(permission)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    mApp.activity,
                    permissionMap!![permission]!!.name
                )
            ) {
                Trace.logFuncWithMessage("Show Request Permission Rationale")
                requestPermission(permissionMap[permission]!!.name)
            } else {
                Trace.logFuncWithMessage("Show Request Permission")
                requestPermission(permissionMap[permission]!!.name)
            }
        } else
            mEventBus.send(PermissionManagerEvent(IPermissionManagerEvent.EventType.Granted))
    }

    private fun requestPermission(name: String) {
        ActivityCompat.requestPermissions(mApp.activity, arrayOf(name), PERMISSIONS_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Trace.logFunc()
        when (requestCode) {
            PERMISSIONS_REQUEST_CODE -> {
                if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mEventBus.send(PermissionManagerEvent(IPermissionManagerEvent.EventType.PermissionStateChanged))
                } else {
                    Trace.logFuncWithMessage("not granted permission")
                    val showRationale =
                        ActivityCompat.shouldShowRequestPermissionRationale(mApp.activity, permissions[0])
                    if (!showRationale) {
                        Trace.logFuncWithMessage("navigate to setting screen to enable permission")
                        mEventBus.send(PermissionManagerEvent(IPermissionManagerEvent.EventType.ShowPermissionRationale))
                    } else
                        mEventBus.send(PermissionManagerEvent(IPermissionManagerEvent.EventType.Rejected))
                }

            }
            else -> mEventBus.send(PermissionManagerEvent(IPermissionManagerEvent.EventType.Rejected))
        }
    }

    override fun onEvent(baseEvent: IBaseEvent) {

    }

    companion object {
        private val PERMISSIONS_REQUEST_CODE = 1232
    }
}
