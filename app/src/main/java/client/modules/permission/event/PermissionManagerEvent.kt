package client.modules.permission.event

import client.base.BaseEvent
import client.modules.permission.PermissionModel

class PermissionManagerEvent : BaseEvent, IPermissionManagerEvent {

    override val eventType: IPermissionManagerEvent.EventType
    override var permission: PermissionModel?

    constructor(mEventType: IPermissionManagerEvent.EventType) {
        this.eventType = mEventType
        permission = null
    }

    constructor(mEventType: IPermissionManagerEvent.EventType, model: PermissionModel) {
        this.eventType = mEventType
        this.permission = model
    }
}
