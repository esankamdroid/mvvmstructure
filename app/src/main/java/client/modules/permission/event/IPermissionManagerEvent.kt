package client.modules.permission.event

import client.interfaces.IBaseEvent
import client.modules.permission.PermissionModel

interface IPermissionManagerEvent : IBaseEvent {

    val eventType: EventType

    val permission: PermissionModel?

    enum class EventType {
        Granted,
        PermissionStateChanged,
        ShowPermissionRationale,
        Rejected
    }
}
