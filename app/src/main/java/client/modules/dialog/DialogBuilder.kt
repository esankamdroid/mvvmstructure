package client.modules.dialog

class DialogBuilder {

    var dialogButtonCallBack: IDialogButtonCallBack? = null
    var dialogDismissCallBack: IDialogDismissCallBack? = null
    var positiveBtnText: String? = null
    var negativeBtnText: String? = null
    var message: String? = null
    var title: String? = null

    fun setNegativeCallBack(dialogDismissCallBack: IDialogDismissCallBack): DialogBuilder {
        this.dialogDismissCallBack = dialogDismissCallBack
        return this
    }

    fun setPositiveCallBack(dialogButtonCallBack: IDialogButtonCallBack): DialogBuilder {
        this.dialogButtonCallBack = dialogButtonCallBack
        return this
    }

    fun setPositiveText(positiveBtnText: String): DialogBuilder {
        this.positiveBtnText = positiveBtnText
        return this
    }

    fun setNegativeText(negativeBtnText: String): DialogBuilder {
        this.negativeBtnText = negativeBtnText
        return this
    }

    fun setMessage(message: String): DialogBuilder {
        this.message = message
        return this
    }

    fun setTitle(title: String): DialogBuilder {
        this.title = title
        return this
    }

}