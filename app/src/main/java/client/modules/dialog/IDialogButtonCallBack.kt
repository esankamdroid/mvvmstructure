package client.modules.dialog

interface IDialogButtonCallBack {
    fun onPositiveClick()
}
