package client.modules.dialog

interface IDialogDismissCallBack {
    fun onDismiss()
}
