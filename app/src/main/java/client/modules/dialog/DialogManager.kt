package client.modules.dialog

import androidx.appcompat.app.AlertDialog
import client.interfaces.IApp
import client.utils.StringUtils
import client.utils.Trace
import com.client.android.R
import javax.inject.Inject


class DialogManager @Inject constructor(private val mApp: IApp) : IDialogManager {

    private var dialog: AlertDialog? = null

    override fun showAlert(builder: DialogBuilder) {
        Trace.logFunc()
        val alertBuilder = AlertDialog.Builder(mApp.activity)
        if (StringUtils.hasContent(builder.title)) {
            alertBuilder.setTitle(builder.title)
        }
        if (StringUtils.hasContent(builder.message)) {
            alertBuilder.setMessage(builder.message)
        }
        if (StringUtils.hasContent(builder.positiveBtnText)) {
            alertBuilder.setPositiveButton(builder.positiveBtnText) { dialogInterface, i ->
                dialogInterface.dismiss()
                if (builder.dialogButtonCallBack != null) {
                    builder.dialogButtonCallBack!!.onPositiveClick()
                }
            }
        }
        if (StringUtils.hasContent(builder.negativeBtnText)) {
            alertBuilder.setNegativeButton(builder.negativeBtnText) { dialogInterface, i ->
                dialogInterface.dismiss()
                if (builder.dialogDismissCallBack != null) {
                    builder.dialogDismissCallBack!!.onDismiss()
                }
            }
        }
        alertBuilder.show()
    }

    override fun show() {
        if (dialog == null) {
            val builder = AlertDialog.Builder(mApp.activity, R.style.AppTheme_ProgressDialog_Theme)
            builder.setView(R.layout.layout_progressbar)
            dialog = builder.show()
            dialog!!.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    override fun dismiss() {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
            dialog = null
        }
    }

    override fun cleanupModule() {
        Trace.logFunc()
        dismiss()
    }
}
