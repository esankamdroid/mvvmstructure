package client.modules.dialog

import client.interfaces.IBaseModule


interface IDialogManager : IBaseModule {

    fun showAlert(builder: DialogBuilder)

    fun show()

    fun dismiss()
}
